import React from 'react';

export default function Evento({ evento }) {
	const { start, logo, description, is_free, name, url } = evento;
	const newLogo = logo
		? logo.url
		: "'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR7IDT7utX0dRl4FNp9QoovBKnFz0b9UZuMWo8D-CBb4SsSBY9c'";
	const newDescri = description.text ? `${description.text.substr(0, 150)}...` : '';
	const isFree = is_free ? 'Gratis' : 'Pago';
	return (
		<div className="col-md-6">
			<div className="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
				<div className="card">
					<img src={newLogo} className="card-img-top img-noticias img-fluid" alt={name.text} />
					<small className="text-muted">{start.timezone}</small>
					<div className="card-body">
						<h5 className="card-title">{name.text}</h5>
						<p className="card-text">{newDescri}</p>
						<a href={url} className="btn btn-outline-dark btn-sm" target="_blank" rel="noopener noreferrer">
							{isFree}
						</a>
					</div>
				</div>
			</div>
		</div>
	);
}
