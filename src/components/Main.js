import React, { Component } from 'react';
import Header from './Header';
import Eventos from './Eventos';
import { API_URL, API_TOKEN, API_LOCALE, API_ORDER } from '../config';
import Axios from 'axios';

export default class Main extends Component {
	state = {
		categorias: [],
		eventos: []
	};
	componentDidMount() {
		this.getCategoriasAxios();
	}

	//Axios
	getEventos = async (evento, idCategoria) => {
		const URL = `${API_URL}/events/search/?q=${evento}&sort_by=${API_ORDER}&categories=${idCategoria}&token=${API_TOKEN}`;
		console.log(URL);
		await Axios.get(URL)
			.then(resp => this.setState({ eventos: resp.data.events }))
			.catch(error => console.log(error));
	};
	// Axios
	getCategoriasAxios = async () => {
		const URL = `${API_URL}/categories/?token=${API_TOKEN}&locale=${API_LOCALE}`;
		await Axios.get(URL)
			.then(resp => this.setState({ categorias: resp.data.categories }))
			.catch(error => console.log(error));
	};

	//Fetch API
	getCategorias = async () => {
		const URL = `${API_URL}/categories/?token=${API_TOKEN}&locale=${API_LOCALE}`;
		await fetch(URL)
			.then(resp => resp.json())
			.then(data => this.setState({ categorias: data.categories }))
			.catch(error => console.log(error));
	};

	//Axios

	render() {
		const { eventos, categorias } = this.state;
		return (
			<div className="container">
				<Header categorias={categorias} getEventos={this.getEventos} />
				<Eventos eventos={eventos} />
			</div>
		);
	}
}
