import React from 'react';
import Evento from './Evento';

export default function Eventos({ eventos }) {
	return <div className="row mb-2 pt-2">{eventos.map((evento, i) => <Evento key={i} evento={evento} />)}</div>;
}
